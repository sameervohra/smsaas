var tips = {};

tips.Vet = {
	"service": "Cow",
	"msg": "Local Vets\n 1. The Andys Vet Clinic - No. 46, Loresho Ridge.\n 2. Dr. Z. A. Cockar - P.O. Box 63838 Limuru Rd."
};

tips.Dentist = {
	"service": "Health",
	"msg": "Dr. Tim visitation schedule\n Aug 1st Kisaruni\n Aug4th Baraka"
}

tips.Maize = {
	"service": "Crops",
	"msg": "Maize\n 1. Maize won't suffer due to upcoming drought.\n 2. Leafy greens complement Maize in integrated farming."
}

module.exports = tips;